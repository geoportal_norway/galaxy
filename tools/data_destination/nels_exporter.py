from galaxy import eggs
import pkg_resources
import os, urllib
import galaxy.model # need to import model before sniff to resolve a circular import dependency

DEFAULT_GALAXY_EXT = "data"
VALID_CHARS = '.-()[]0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ '
host  = ''
    
def histLabelWithExt(histItem):
    label, ext = histItem[1:]
    if label.endswith('.' + ext):
        return label
    return '.'.join([label, ext])

def uploadToNels(galaxyToolDataDir, filePath, nelsId, outfn, histItems):
    datasetsDir = '/'.join(histItems[0][0].split('/')[:-1]) + '/'
    username, sshKey = getSshKey(galaxyToolDataDir, nelsId)
    if filePath[-1] !='/':
        filePath += '/'
    sshKeyFn = datasetsDir+'%s.txt'%username
    with open(sshKeyFn, 'wb') as sshObj:
        sshObj.write(sshKey)
        os.system('chmod 0600 '+sshKeyFn)
    
    with open(outfn, 'w') as outFileObj: #.write(str(histItems) +'\n\n'+filePath)
        for histItem in histItems:
            transferFileToNels(histItem, filePath, sshKeyFn, username)
            print>>outFileObj, 'Transferred "%s" to my area at NELS ( "%s" ) <br/>' % (histLabelWithExt(histItem), filePath)
    
    os.remove(sshKeyFn)
    return True

def getNelsStorageConfig(galaxyToolDataDir):
    lines = []
    cwd = os.getcwd()
    rootDir = galaxyToolDataDir #cwd[:cwd.rfind('tool-data')]
    with open(rootDir + '/nels_storage_config.loc') as f:
        lines = [v.strip() for v in f if v.strip() != '' and v.strip()[0] != '#']
    return lines

def getSshKey(galaxyToolDataDir, nelsId):
    global host
    pkg_resources.require( "suds" )
    pkg_resources.require( "nels.storage" )
    from nels.storage.config import config as storage_config
    from nels import storage
    host, wsdl, clientKey = getNelsStorageConfig(galaxyToolDataDir)
    storage_config.set_wsdl(wsdl)                                                 
    storage_config.set_client_key(clientKey)      
    return storage.get_ssh_credential(nelsId, nelsId)

def transferFileToNels(histItem, filePath, sshKeyFn, username):
    histFile = histItem[0]
    #ending = histFile.split('.')[-1]
    label = urllib.quote(histLabelWithExt(histItem), ' ')
    destFile = (filePath+label).replace('//','/').replace(' ','\ ')
    #host  = 'barkbille.bccs.uib.no'
    #print 'scp -o BatchMode=yes -i %s %s %s@%s:%s' % (sshKeyFn, histFile, username, host, destFile)
    os.system( 'scp -o BatchMode=yes -i %s %s "%s@%s:%s"' % (sshKeyFn, histFile, username, host, destFile) )
        
def checkArguments(argList):
    argSize = len(argList)
    if argSize<8 or (argSize-5)%3 != 0:
        outFn = argList[4]

        with open(outFn,'w') as outFile:
            print>>outFile, 'No history elements selected to be exported to NELS repository<br/>Please rerun the tool ' \
                            'and select at least 1 dataset to be exported form your history to the central repository' \
                            '<br/><br/>Arguments for run:<br/>'
            labels = ['Galaxy tool-data path', 'tool used', 'remote file path', 'nelsId', 'output file path']
            print>>outFile, '<br/> '.join(['%s: %s'%(labels[i], argList[i]) for i in range(min(argSize, len(labels)))])+'<br/>'

            if argsize>5:
                print>>outFile, 'Selected datasets for export: '+repr(argList[5:])

        return False
    
    return True

if __name__ == '__main__':
    import sys
    
    if checkArguments(sys.argv):
        galaxyToolDataDir = sys.argv[1]
        filePath, nelsId, outFn = sys.argv[2:5]
        histItems = [(sys.argv[i], sys.argv[i+1], sys.argv[i+2]) for i in range(5, len(sys.argv), 3)]
        uploadToNels(galaxyToolDataDir, filePath, nelsId, outFn, histItems)
