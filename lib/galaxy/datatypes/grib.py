"""
Grib classes
"""

import data
import logging
import binascii
import os
import shutil
import struct
import subprocess
import tempfile
import re
import zipfile
import tarfile
from gribapi import grib_count_in_file

from galaxy import eggs
eggs.require( "bx-python" )

from bx.seq.twobit import TWOBIT_MAGIC_NUMBER, TWOBIT_MAGIC_NUMBER_SWAP, TWOBIT_MAGIC_SIZE

from galaxy.util import sqlite
from galaxy.datatypes.metadata import MetadataElement, MetadataParameter, ListParameter, DictParameter
from galaxy.datatypes import metadata
from galaxy.datatypes.binary import Binary
import dataproviders

log = logging.getLogger(__name__)

class Grib ( Binary ):
    """Class for generic GRIB format"""
    file_ext = "grib"

    def __init__( self, **kwd ):
        Binary.__init__( self, **kwd )


    @staticmethod
    def check_grib( file_path ):
        header = open( file_path,"rb" )
        tag = header.read(4)
        if binascii.b2a_hex( tag ) == binascii.hexlify( 'GRIB' ):
            return True
        return False

    def sniff( self, filename ):
        # GRIB is a self-content format
        log.info("Sniff Grib: "+str(filename))
        try:
            if Grib.check_grib( filename ):
                return True
            else:
                return False
        except Exception, err :
            log.info(err)
            return False

    def set_peek( self, dataset, is_multi_byte=False ):
        if not dataset.dataset.purged:
            dataset.peek  = "Binary GRIB file"
            dataset.blurb = data.nice_size( dataset.get_size() )
        else:
            dataset.peek = 'file does not exist'
            dataset.blurb = 'file purged from disk'

    def display_peek( self, dataset ):
        try:
            return dataset.peek
        except:
            return "Binary GRIB file (%s)" % ( data.nice_size( dataset.get_size() ) )

    def display_data(self, trans, dataset, preview=False, filename=None, to_ext=None, size=None, offset=None, **kwd):
        log.info("grib display_data") 
        if to_ext or not preview:
            log.info("grib to_ext or not preview") 
            return self._serve_raw(trans, dataset, to_ext)
        else:
          log.info("grib grib_ls") 
          ofile_handle = tempfile.NamedTemporaryFile(delete=False)
          ofilename = ofile_handle.name
          ofile_handle.close()
          try:
             cmd = 'grib_ls %s | tail -n +2 | head -n -3 >  %s' % (dataset.file_name, ofilename)
             log.info("Calling command %s" % cmd)
             subprocess.call(cmd, shell=True)
             trans.response.set_content_type( "text/html" )
             return trans.stream_template_mako( "/dataset/file.mako",
                                            data = open( ofilename).read(),
                                            dataset = dataset)
          except:
             ofilename = dataset.file_name
             log.exception( 'Command "%s" failed. Could not grib_ls the GRIB file.' % cmd )
 
Binary.register_sniffable_binary_format("grib", "grib", Grib)

