"""
openIFS datatypes
Anne Fouilloux
University of Oslo
for running openIFS with galaxy portal 

oIFSin is a class for OpenIFS input data 
oIFSout is a class containing OpenIFS outputs

July 9 2015
"""

import logging, os, sys, time, tempfile, shutil, string, glob
import data
import mimetypes
from galaxy import util
from cgi import escape
import urllib, binascii
from galaxy.web import url_for
from galaxy.datatypes import metadata
from galaxy.datatypes.metadata import MetadataElement
from galaxy.datatypes.binary import Binary
from galaxy.datatypes.images import Html, Image
from galaxy.datatypes.grib import Grib
from galaxy.datatypes.data import Text
from galaxy.util.hash_util import *
from galaxy.util.sanitize_html import sanitize_html

log = logging.getLogger(__name__)
verbose = True


class oIFS(Html):
    """
    base class to use for openifs datatypes
    derived from html - composite datatype elements
    stored in extra files path
    """

    MetadataElement( name="base_name", desc="base name for OpenIFS dataset", default='OpenIFSData',
    readonly=True, set_in_upload=True)

    composite_type = 'auto_primary_file'
    allow_datatype_change = False
    file_ext = 'oifs'

    log.info("OpenIFS class")

    def generate_primary_file( self, dataset = None ):
        rval = ['<html><head><title><h1><b>OpenIFS generated</b></h1></title></head><p/>']
        rval.append('<div>This composite dataset is composed of the following files:<p/><ul>')
        for composite_name, composite_file in self.get_composite_files( dataset = dataset ).iteritems():
            fn = composite_name
            opt_text = ''
            if composite_file.optional:
                opt_text = ' (optional)'
            if composite_file.get('description'):
                rval.append( '<li><a href="%s" type="application/binary">%s (%s)</a>%s</li>' % ( fn, fn, composite_file.get('description'), opt_text ) )
            else:
                rval.append( '<li><a href="%s" type="application/binary">%s</a>%s</li>' % ( fn, fn, opt_text ) )
        rval.append( '</ul></div></html>' )
        return "\n".join( rval )

    def regenerate_primary_file(self,dataset):
        """
        cannot do this until we are setting metadata
        """
        bn = dataset.metadata.base_name
        efp = dataset.extra_files_path
        flist = os.listdir(efp)
        rval = ['<html><head><title><h1><b>OpenIFS generated</b></h1></title></head><body><p/>Composite %s contains:<p/><ul>' % (dataset.name)]
        for i,fname in enumerate(flist):
            sfname = os.path.split(fname)[-1]
            f,e = os.path.splitext(fname)
            rval.append( '<li><a href="%s">%s</a></li>' % ( sfname, sfname) )
        rval.append( '</ul></body></html>' )
        f = file(dataset.file_name,'w')
        f.write("\n".join( rval ))
        f.write('\n')
        f.close()

    def get_mime(self):
        """Returns the mime type of the datatype"""
        return 'text/html'


    def set_meta( self, dataset, **kwd ):

        """
        for OpenIFS

        """
        Html.set_meta( self, dataset, **kwd )
        if kwd.get('overwrite') == False:
            if verbose:
                log.debug('@@@openIFS set_meta called with overwrite = False')
            return True
        try:
            efp = dataset.extra_files_path
        except:
            if verbose:
               log.debug('@@@openIFS set_meta failed %s - dataset %s has no efp ?' % (sys.exc_info()[0], dataset.name))
            return False
        try:
            flist = os.listdir(efp)
        except:
            if verbose: log.debug('@@@openIFS set_meta failed %s - dataset %s has no efp ?' % (sys.exc_info()[0],dataset.name))
            return False
        if len(flist) == 0:
            if verbose:
                log.debug('@@@openIFS set_meta failed - %s openIFS input %s is empty?' % (dataset.name,efp))
            return False
        self.regenerate_primary_file(dataset)
        if not dataset.info:
                dataset.info = 'Galaxy openIFS datatype object'
        if not dataset.blurb:
               dataset.blurb = 'Composite file - openIFS Galaxy toolkit'
        return True

    def display_data(self, trans, data, preview=False, filename=None, to_ext=None, size=None, offset=None, **kwd):
        """ 
          Display OIFS datasets
        """
        log.info("Display OIFS datasets " + str(filename))
        if to_ext:
            log.info("oIFS to_ext (Download)")
            return self._archive_composite_dataset( trans, data, **kwd )
        elif not preview and filename:
            log.info("oIFS not preview is true and filename" + filename)
            # For files in extra_files_path
            if len(filename)>10:
              if filename[0:10]=='[download]':
                log.info("oIFS filename > 10 and download data.file_name = " + str(data.file_name))
                data.file_name = filename[10:]
                file_path = trans.app.object_store.get_filename(data.dataset, extra_dir='dataset_%s_files' % data.dataset.id, alt_name=filename[10:])
                log.info("oIFS filename > 10 and download " + str(file_path))
                if os.path.exists( file_path ):
                    if os.path.isdir( file_path ):
                        return trans.show_error_message( "Directory listing is not allowed." ) #TODO: Reconsider allowing listing of directories?
                    mime, encoding = mimetypes.guess_type( file_path )
                    if not mime:
                        try:
                            mime = trans.app.datatypes_registry.get_mimetype_by_extension( file_path.split( "." )[-1] )
                        except:
                            mime = "text/plain"
                    self._clean_and_set_mime_type( trans, mime )
                    return open( file_path )
                return trans.show_error_message( "File " + filename[10:] + " does not exist!" )

            file_path = trans.app.object_store.get_filename(data.dataset, extra_dir='dataset_%s_files' % data.dataset.id, alt_name=filename)
            log.info("oIFS file_path " + file_path)
            if os.path.exists( file_path ):
                log.info("oIFS file_path exists")
                if os.path.isdir( file_path ):
                    return trans.show_error_message( "Directory listing is not allowed." ) #TODO: Reconsider allowing listing of directories?
                if Grib.check_grib( file_path ):
                   data_type = 'grib'
                   ext = "grib"
                   log.info("oIFS GRIB file")
                   grib =  Grib()
                   to_ext = False
                   preview = True
                   data.file_name = file_path
                   return grib.display_data(trans, data, preview, file_path, to_ext, size, **kwd)
                trans.response.set_content_type( "text/html" )
                return trans.stream_template_mako( "/dataset/file.mako",
                                            data = open( file_path).read(),
                                            dataset = data)
            log.info("oIFS file_path does not exist")
            return trans.show_error_message( "Could not find '%s' on the extra files path %s." % ( filename, file_path ) )
        elif not preview:
            log.info("oIFS not_preview and not filename")
            return self._serve_raw(trans, data, to_ext)
        else:
            log.info("oIFS display mime is " + data.get_mime())
            if trans.app.config.sanitize_all_html and trans.response.get_content_type() == "text/html":
                    # Sanitize anytime we respond with plain text/html content.
                return sanitize_html(open( data.file_name ).read())
            return open( data.file_name )


class oIFSexp(oIFS):
    def __init__( self, **kwd ):
        log.info("OpenIFS experiment class")
        oIFS.__init__(self, **kwd)
        self.expid='None'
        self.start_date='YYYYMMDD'
        self.end_date='YYYYMMDD'
        self.times='00'

    @staticmethod
    def check_oIFSexp( file_path ):
        log.info("check_oIFSexp")
        # should contain OIFS_EXPID, OIFS_SDATE, OIFS_EDATE and OIFS_TIMES
        try:
# read file and search for OIFS_EXPID, OIFS_SDATE, OIFS_EDATE and OIFS_TIMES
            f = open(file_path)
            is_oIFSexp = False
            for line in f:
                if "OIFS_EXPID" in line:
                    tmp=line.split('=')[1]
                    log.info("OIFS_EXPID: " + tmp.strip())
#                    self.expid = tmp.strip()
                    is_oIFSexp = True
                if "OIFS_SDATE" in line:
                    tmp=line.split('=')[1]
                    log.info("OIFS_SDATE: " + tmp.strip())
#                    self.start_date= tmp.strip()
                    is_oIFSexp = True
                if "OIFS_EDATE" in line:
                    tmp=line.split('=')[1]
                    log.info("OIFS_EDATE: " + tmp.strip())
#                    self.end_date = tmp.strip()
                    is_oIFSexp = True
            f.close()
            return is_oIFSexp
        except:
            return False

    def sniff( self, filename ):

        try:
            if oIFSexp.check_oIFSexp( filename ):
                return True
            return False
        except Exception, err :
            log.info(err)
            return False

    def display_data(self, trans, data, preview=False, filename=None, to_ext=None, size=None, offset=None, **kwd):
        """ 
          Display OIFS exp
        """
        log.info("Display OIFS exp " + str(filename))
        if to_ext or not preview:
            return self._serve_raw(trans, data, to_ext)
        else:
            trans.response.set_content_type( "text/html" )
            return trans.stream_template_mako( "/dataset/file.mako",
                                            data = open( data.file_name).read(),
                                            dataset = data)

class oIFSin(oIFS):
    """
    openIFS inputs
    """
    file_ext="oifsin"

    def __init__( self, **kwd ):
        log.info("OpenIFS inputs class")
        oIFS.__init__(self, **kwd)
        self.version=None
        self.start_date=None
        self.end_date=None

    @staticmethod
    def check_oIFSin( file_path ):
        # should have a list of files
        try:
# read file and search for <title>OpenIFS inputs generated by getdata.py</title>
            f = open(file_path)
	    is_oIFSin = False
            for line in f:
                if "<title><h1><b>OpenIFS inputs generated by getdata.py</b></h1></title>" in line:
		    is_oIFSin = True
            f.close()
            return is_oIFSin
        except:
            return False

    def sniff( self, filename ):

        try:
            if oIFSin.check_oIFSin( filename ):
                return True
            return False
        except Exception, err :
            log.info(err)
            return False

    def set_peek( self, dataset, **kwd ):
        log.info("oIFSin set_peek")
        if not dataset.dataset.purged:
            dataset.peek  = "OpenIFS input file"
            dataset.blurb = data.nice_size( dataset.get_size() )
        else:
            dataset.peek = 'file does not exist'
            dataset.blurb = 'file purged from disk'

    def display_peek( self, dataset ):
        log.info("oIFSin display_peek")
        try:
            return dataset.peek
        except:
            return "OpenIFS input file (%s)" % ( data.nice_size( dataset.get_size() ) )



class oIFSout(oIFS):
    """
    openIFS outputs
    """
    file_ext="oifsout"

    def __init__( self, **kwd ):
        log.info("OpenIFS outputs class")
        oIFS.__init__(self, **kwd)
        self.version=None
        self.start_date=None
        self.end_date=None


    @staticmethod
    def check_oIFSout( file_path ):
        # should have a list of files
        try:
# read file and search for <title>OpenIFS outputs generated by getdata.py</title>
            f = open(file_path)
	    is_oIFSout = False
            for line in f:
                if "<title><h1><b>OpenIFS outputs generated when running openIFS forecast model</b></h1></title>" in line:
		    is_oIFSout = True
            f.close()
            return is_oIFSout
        except:
            return False

    def sniff( self, filename ):

        try:
            if oIFSout.check_oIFSout( filename ):
                return True
            return False
        except Exception, err :
            log.info(err)
            return False

    def set_peek( self, dataset, **kwd ):
        if not dataset.dataset.purged:
            dataset.peek  = "openIFS output file"
            dataset.blurb = data.nice_size( dataset.get_size() )
        else:
            dataset.peek = 'file does not exist'
            dataset.blurb = 'file purged from disk'

    def display_peek( self, dataset ):
        log.info("oIFSout display_peek")
        try:
            return dataset.peek
        except:
            return "OpenIFS output file (%s)" % ( data.nice_size( dataset.get_size() ) )

