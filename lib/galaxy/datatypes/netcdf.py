"""
netCDF classes
"""

import data
import logging
import os
import shutil
import struct
import subprocess
import tempfile
import re
import zipfile
import tarfile

from galaxy import eggs
eggs.require( "bx-python" )

from bx.seq.twobit import TWOBIT_MAGIC_NUMBER, TWOBIT_MAGIC_NUMBER_SWAP, TWOBIT_MAGIC_SIZE

from galaxy.util import sqlite
from galaxy.datatypes.metadata import MetadataElement, MetadataParameter, ListParameter, DictParameter
from galaxy.datatypes import metadata
from galaxy.datatypes.binary import Binary
import dataproviders

log = logging.getLogger(__name__)

class netCDF ( Binary ):
    """Class for generic netCDF format"""
    file_ext = "nc"

    def __init__( self, **kwd ):
        log.info("netCDF init")
        Binary.__init__( self, **kwd )
        self.schema=None

    @staticmethod
    def check_netcdf( file_path ):
        log.info("netCDF check_netcdf")
        # NetCDF magic: classic, 64 bit, netcdf4
        netcdf_headers = ['\x43\x44\x46\x01','\x43\x44\x46\x02','\x89\x48\x44\x46']
        try:
            header = open( file_path ).read(4)
            if header in netcdf_headers:
                return True
            return False
        except:
            return False

    def sniff( self, filename ):
        log.info("netCDF Sniff: "+str(filename))
        try:
            if netCDF.check_netcdf( filename ):
                return True
            return False
        except Exception, err :
            log.info(err)
            return False


    def set_peek( self, dataset, is_multi_byte=False ):
        log.info("netCDF set_peek")
        if not dataset.dataset.purged:
            dataset.peek  = "Binary netCDF file"
            dataset.blurb = data.nice_size( dataset.get_size() )
        else:
            dataset.peek = 'file does not exist'
            dataset.blurb = 'file purged from disk'

    def display_peek( self, dataset ):
        log.info("netCDF display_peek")
        try:
            return dataset.peek
        except:
            return "Binary netCDF file (%s)" % ( data.nice_size( dataset.get_size() ) )

    def display_data(self, trans, dataset, preview=False, filename=None, to_ext=None, size=None, offset=None, **kwd):
        log.info("netCDF display_data")
        if to_ext or not preview:
            return self._serve_raw(trans, dataset, to_ext)
        else:
          ofile_handle = tempfile.NamedTemporaryFile(delete=False)
          ofilename = ofile_handle.name
          ofile_handle.close()
          try:
             cmd = "ncdump -h %s | sed 's/^   //g' | sed '1d;$d' >  %s" % (dataset.file_name, ofilename)
             log.info("Calling command %s" % cmd)
             subprocess.call(cmd, shell=True)
             trans.response.set_content_type( "text/html" )
             return trans.stream_template_mako( "/dataset/file.mako",
                                            data = open( ofilename).read(),
                                            dataset = dataset)
          except:
             ofilename = dataset.file_name
             log.exception( 'Command "%s" failed. Could not grib_ls the GRIB file.' % cmd )
 
Binary.register_sniffable_binary_format("nc", "nc", netCDF)

