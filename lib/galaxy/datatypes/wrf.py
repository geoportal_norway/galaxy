"""
WRF datatypes
Anne Fouilloux
University of Oslo
for running WRF with galaxy portal 

WRFexp is a class for WRF experiment setup
WRFin is a class for WRF input data 
WRFout is a class containing WRF outputs

Sep 1 2015
"""

import logging, os, sys, time, tempfile, shutil, string, glob
import data
import mimetypes
from galaxy import util
from cgi import escape
import urllib, binascii
from galaxy.web import url_for
from galaxy.datatypes import metadata
from galaxy.datatypes.metadata import MetadataElement
from galaxy.datatypes.binary import Binary
from galaxy.datatypes.images import Html, Image
from galaxy.datatypes.grib import Grib
from galaxy.datatypes.data import Text
from galaxy.util.hash_util import *
from galaxy.util.sanitize_html import sanitize_html

log = logging.getLogger(__name__)
verbose = True


class WRF(Html):
    """
    base class to use for wrf datatypes
    derived from html - composite datatype elements
    stored in extra files path
    """

    MetadataElement( name="base_name", desc="base name for WRF dataset", default='WRFData',
    readonly=True, set_in_upload=True)

    composite_type = 'auto_primary_file'
    allow_datatype_change = False
    file_ext = 'wrf'

    log.info("WRF class")

    def generate_primary_file( self, dataset = None ):
        rval = ['<html><head><title><h1><b>WRF generated</b></h1></title></head><p/>']
        rval.append('<div>This composite dataset is composed of the following files:<p/><ul>')
        for composite_name, composite_file in self.get_composite_files( dataset = dataset ).iteritems():
            fn = composite_name
            opt_text = ''
            if composite_file.optional:
                opt_text = ' (optional)'
            if composite_file.get('description'):
                rval.append( '<li><a href="%s" type="application/binary">%s (%s)</a>%s</li>' % ( fn, fn, composite_file.get('description'), opt_text ) )
            else:
                rval.append( '<li><a href="%s" type="application/binary">%s</a>%s</li>' % ( fn, fn, opt_text ) )
        rval.append( '</ul></div></html>' )
        return "\n".join( rval )

    def regenerate_primary_file(self,dataset):
        """
        cannot do this until we are setting metadata
        """
        bn = dataset.metadata.base_name
        efp = dataset.extra_files_path
        flist = os.listdir(efp)
        rval = ['<html><head><title><h1><b>WRF generated</b></h1></title></head><body><p/>Composite %s contains:<p/><ul>' % (dataset.name)]
        for i,fname in enumerate(flist):
            sfname = os.path.split(fname)[-1]
            f,e = os.path.splitext(fname)
            rval.append( '<li><a href="%s">%s</a></li>' % ( sfname, sfname) )
        rval.append( '</ul></body></html>' )
        f = file(dataset.file_name,'w')
        f.write("\n".join( rval ))
        f.write('\n')
        f.close()

    def get_mime(self):
        """Returns the mime type of the datatype"""
        return 'text/html'


    def set_meta( self, dataset, **kwd ):

        """
        for WRF

        """
        Html.set_meta( self, dataset, **kwd )
        if kwd.get('overwrite') == False:
            if verbose:
                log.debug('@@@WRF set_meta called with overwrite = False')
            return True
        try:
            efp = dataset.extra_files_path
        except:
            if verbose:
               log.debug('@@@WRF set_meta failed %s - dataset %s has no efp ?' % (sys.exc_info()[0], dataset.name))
            return False
        try:
            flist = os.listdir(efp)
        except:
            if verbose: log.debug('@@@WRF set_meta failed %s - dataset %s has no efp ?' % (sys.exc_info()[0],dataset.name))
            return False
        if len(flist) == 0:
            if verbose:
                log.debug('@@@WRF set_meta failed - %s WRF input %s is empty?' % (dataset.name,efp))
            return False
        self.regenerate_primary_file(dataset)
        if not dataset.info:
                dataset.info = 'Galaxy WRF datatype object'
        if not dataset.blurb:
               dataset.blurb = 'Composite file - WRF Galaxy toolkit'
        return True

    def display_data(self, trans, data, preview=False, filename=None, to_ext=None, size=None, offset=None, **kwd):
        """ 
          Display WRF datasets
        """
        log.info("Display WRF datasets " + str(filename))
        if to_ext:
            log.info("WRF to_ext (Download)")
            return self._archive_composite_dataset( trans, data, **kwd )
        elif not preview and filename:
            log.info("WRF not preview is true and filename" + filename)
            # For files in extra_files_path
            if len(filename)>10:
              if filename[0:10]=='[download]':
                log.info("WRF filename > 10 and download data.file_name = " + str(data.file_name))
                data.file_name = filename[10:]
                file_path = trans.app.object_store.get_filename(data.dataset, extra_dir='dataset_%s_files' % data.dataset.id, alt_name=filename[10:])
                log.info("WRF filename > 10 and download " + str(file_path))
                if os.path.exists( file_path ):
                    if os.path.isdir( file_path ):
                        return trans.show_error_message( "Directory listing is not allowed." ) #TODO: Reconsider allowing listing of directories?
                    mime, encoding = mimetypes.guess_type( file_path )
                    if not mime:
                        try:
                            mime = trans.app.datatypes_registry.get_mimetype_by_extension( file_path.split( "." )[-1] )
                        except:
                            mime = "text/plain"
                    self._clean_and_set_mime_type( trans, mime )
                    return open( file_path )
                return trans.show_error_message( "File " + filename[10:] + " does not exist!" )

            file_path = trans.app.object_store.get_filename(data.dataset, extra_dir='dataset_%s_files' % data.dataset.id, alt_name=filename)
            log.info("WRF file_path " + file_path)
            if os.path.exists( file_path ):
                log.info("WRF file_path exists")
                if os.path.isdir( file_path ):
                    return trans.show_error_message( "Directory listing is not allowed." ) #TODO: Reconsider allowing listing of directories?
                if Grib.check_grib( file_path ):
                   data_type = 'grib'
                   ext = "grib"
                   log.info("WRF GRIB file")
                   grib =  Grib()
                   to_ext = False
                   preview = True
                   data.file_name = file_path
                   return grib.display_data(trans, data, preview, file_path, to_ext, size, **kwd)
                trans.response.set_content_type( "text/html" )
                return trans.stream_template_mako( "/dataset/file.mako",
                                            data = open( file_path).read(),
                                            dataset = data)
            log.info("WRF file_path does not exist")
            return trans.show_error_message( "Could not find '%s' on the extra files path %s." % ( filename, file_path ) )
        elif not preview:
            log.info("WRF not_preview and not filename")
            return self._serve_raw(trans, data, to_ext)
        else:
            log.info("WRF display mime is " + data.get_mime())
            if trans.app.config.sanitize_all_html and trans.response.get_content_type() == "text/html":
                    # Sanitize anytime we respond with plain text/html content.
                return sanitize_html(open( data.file_name ).read())
            return open( data.file_name )


class WRFexp(WRF):
    def __init__( self, **kwd ):
        log.info("WRF experiment class")
        WRF.__init__(self, **kwd)
        self.expid='None'
        self.start_date='YYYYMMDD'
        self.end_date='YYYYMMDD'

    @staticmethod
    def check_WRFexp( file_path ):
        log.info("check_WRFexp")
        # should contain WRF_EXPID
        try:
# read file and search for WRF_EXPID
            f = open(file_path)
            is_WRFexp = False
            for line in f:
                if "WRF_EXPID" in line:
                    tmp=line.split('=')[1]
                    log.info("WRF_EXPID: " + tmp.strip())
                    is_WRFexp = True
            f.close()
            return is_WRFexp
        except:
            return False

    def sniff( self, filename ):

        try:
            if WRFexp.check_WRFexp( filename ):
                return True
            return False
        except Exception, err :
            log.info(err)
            return False

    def display_data(self, trans, data, preview=False, filename=None, to_ext=None, size=None, offset=None, **kwd):
        """ 
          Display WRF exp
        """
        log.info("Display WRF exp " + str(filename))
        if to_ext or not preview:
            return self._serve_raw(trans, data, to_ext)
        else:
            trans.response.set_content_type( "text/html" )
            return trans.stream_template_mako( "/dataset/file.mako",
                                            data = open( data.file_name).read(),
                                            dataset = data)

class WRFgeoTBL(WRF):
    def __init__( self, **kwd ):
        log.info("WRF geogrid table (TBL) class")
        WRF.__init__(self, **kwd)

    @staticmethod
    def check_WRFgeoTBL( file_path ):
        log.info("check_WRFgeoTBL")
        # should contain Name and for each Name: priority, dest_type, interp_option
        try:
# read file and search for the right items
            f = open(file_path)
            is_WRFgeoTBL = False
            new_item = False
            nitems = 0 
            for line in f:
                if "============" in line:
                    if new_item and is_priority and is_dest_type and is_interp and is_path:
                       nitems = nitems + 1
                    new_item = False
                    is_priority = False
                    is_dest_type = False
                    is_path = False 
                    is_interp = False
                if "name" in line:
                    new_item = True
                if new_item:
                   if "priority" in line:
                      is_priority= True
                   if "dest_type" in line:
                      is_dest_type= True
                   if "interp_option" in line:
                      is_interp= True
                   if "rel_path" or "abs_path" in line:
                      is_path= True
            f.close()
            if nitems > 0:
                   is_WRFgeoTBL = True
            return is_WRFgeoTBL
        except:
            return False

    def sniff( self, filename ):

        try:
            if WRFexp.check_WRFgeoTBL( filename ):
                return True
            return False
        except Exception, err :
            log.info(err)
            return False

    def display_data(self, trans, data, preview=False, filename=None, to_ext=None, size=None, offset=None, **kwd):
        """ 
          Display WRF geoTBL
        """
        log.info("Display WRF GEOGRID TBL " + str(filename))
        if to_ext or not preview:
            return self._serve_raw(trans, data, to_ext)
        else:
            trans.response.set_content_type( "text/html" )
            return trans.stream_template_mako( "/dataset/file.mako",
                                            data = open( data.file_name).read(),
                                            dataset = data)

class WRFmetTBL(WRF):
    def __init__( self, **kwd ):
        log.info("WRF metgrid table (TBL) class")
        WRF.__init__(self, **kwd)

    @staticmethod
    def check_WRFmetTBL( file_path ):
        log.info("check_WRFmetTBL")
        # should contain Name and for each Name: priority, dest_type, interp_option
        try:
# read file and search for the right items
            f = open(file_path)
            is_WRFmetTBL = False
            new_item = False
            nitems = 0 
            for line in f:
                if "============" in line:
                    if new_item:
                       nitems = nitems + 1
                    new_item = False
                if "name" in line:
                    new_item = True
            f.close()
            if nitems > 0:
                   is_WRFmetTBL = True
            return is_WRFmetTBL
        except:
            return False

    def sniff( self, filename ):

        try:
            if WRFexp.check_WRFmetTBL( filename ):
                return True
            return False
        except Exception, err :
            log.info(err)
            return False

    def display_data(self, trans, data, preview=False, filename=None, to_ext=None, size=None, offset=None, **kwd):
        """ 
          Display WRF metTBL
        """
        log.info("Display WRF METGRID TBL " + str(filename))
        if to_ext or not preview:
            return self._serve_raw(trans, data, to_ext)
        else:
            trans.response.set_content_type( "text/html" )
            return trans.stream_template_mako( "/dataset/file.mako",
                                            data = open( data.file_name).read(),
                                            dataset = data)

class WRFin(WRF):
    """
    WRFin inputs
    """
    file_ext="wrfin"

    def __init__( self, **kwd ):
        log.info("WRF inputs class")
        WRF.__init__(self, **kwd)
        self.version=None
        self.start_date=None
        self.end_date=None

    @staticmethod
    def check_WRFin( file_path ):
        # should have a list of files
        try:
# read file and search for <title>WRF inputs</title>
            f = open(file_path)
	    is_WRFin = False
            for line in f:
                if "<title><h1><b>WRF inputs</b></h1></title>" in line:
		    is_WRFin = True
            f.close()
            return is_WRFin
        except:
            return False

    def sniff( self, filename ):

        try:
            if WRFin.check_WRFin( filename ):
                return True
            return False
        except Exception, err :
            log.info(err)
            return False

    def set_peek( self, dataset, **kwd ):
        log.info("WRFin set_peek")
        if not dataset.dataset.purged:
            dataset.peek  = "WRF input file"
            dataset.blurb = data.nice_size( dataset.get_size() )
        else:
            dataset.peek = 'file does not exist'
            dataset.blurb = 'file purged from disk'

    def display_peek( self, dataset ):
        log.info("WRFin display_peek")
        try:
            return dataset.peek
        except:
            return "WRF input file (%s)" % ( data.nice_size( dataset.get_size() ) )



class WRFout(WRF):
    """
    WRF outputs
    """
    file_ext="wrfout"

    def __init__( self, **kwd ):
        log.info("WRF outputs class")
        WRF.__init__(self, **kwd)
        self.version=None
        self.start_date=None
        self.end_date=None


    @staticmethod
    def check_WRFout( file_path ):
        # should have a list of files
        try:
# read file and search for <title>WRF outputs</title>
            f = open(file_path)
	    is_WRFout = False
            for line in f:
                if "<title><h1><b>WRF outputs generated when running WRF model</b></h1></title>" in line:
		    is_WRFout = True
            f.close()
            return is_WRFout
        except:
            return False

    def sniff( self, filename ):

        try:
            if WRFout.check_WRFout( filename ):
                return True
            return False
        except Exception, err :
            log.info(err)
            return False

    def set_peek( self, dataset, **kwd ):
        if not dataset.dataset.purged:
            dataset.peek  = "WRF output file"
            dataset.blurb = data.nice_size( dataset.get_size() )
        else:
            dataset.peek = 'file does not exist'
            dataset.blurb = 'file purged from disk'

    def display_peek( self, dataset ):
        log.info("WRFout display_peek")
        try:
            return dataset.peek
        except:
            return "WRF output file (%s)" % ( data.nice_size( dataset.get_size() ) )

