#!/bin/sh

module purge

module load initenv
module load openifs/38r1v04
module load metview
module load ncl/6.3.0
module load python
source activate py2galaxy
## set required variables. Very site specific, so change these to suit your needs.

echo "Loading startup env variables  ... from ./startup_settings"

# add additional tools
export ADDITIONAL_TOOLS=/uio/kant/geo-adm-u1/annefou/Docs/Install/galaxy/abel/geoportal-galaxy-AF/additional_tools
export PATH=/uio/kant/geo-adm-u1/annefou/Docs/Install/galaxy/abel/geoportal-galaxy-AF/additional_tools:$PATH

# set temportal directory for file upload
export CLUST_TEMP=/mn/vann/it-scratch/galaxy/tmp

# set GALAXY_LIB
export GALAXY_LIB="/uio/kant/geo-adm-u1/annefou/Docs/Install/galaxy/abel/geoportal-galaxy-AF/galaxy-dist/lib"
echo "GALAXY_LIB:" $GALAXY_LIB


# to activate the slurm-specific code in galaxy
export GALAXY_SLURM="0"
echo "GALAXY_SLURM : "$GALAXY_SLURM

# set PYTHONPATH
#export PYTHONPATH=/home/galaxy/sp:$PYTHONPATH
export PYTHONPATH=$GALAXY_LIB:$PYTHONPATH
export PYTHONPATH=/usr/lib64/python2.6/site-packages:$PYTHONPATH
export PYTHONPATH=/uio/kant/geo-adm-u1/annefou/Docs/Install/galaxy/abel/geoportal-galaxy-AF/galaxy-dist/eggs:$PYTHONPATH
export PYTHONPATH=/uio/kant/geo-adm-u1/annefou/Docs/Install/galaxy/abel/geoportal-galaxy-AF/additional_python_galaxy_packs:$PYTHONPATH
export PYTHONPATH=/uio/kant/geo-adm-u1/annefou/Docs/Install/galaxy/abel/geoportal-galaxy-AF/python/lib/python2.6/site-packages:$PYTHONPATH
export PYTHONPATH=/site/opt/Magics++/2.22.0/lib/python2.6/site-packages:$PYTHONPATH
echo "PYTHONPATH : "$PYTHONPATH


####################
# Specific files and directories
####################

## used in tools/myTools/jobstate.py
export DATADIR="/mn/vann/it-scratch/galaxy/data/database_galaxy_geoportal_dev/"
echo "DATADIR : " $DATADIR

