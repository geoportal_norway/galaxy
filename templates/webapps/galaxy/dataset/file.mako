<%inherit file="/base.mako"/>
<%namespace file="/dataset/display.mako" import="render_deleted_data_message" />

${ render_deleted_data_message( dataset ) }

<pre>
${ util.unicodify( data ) | h }
</pre>
